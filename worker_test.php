<?php

$command = "php -r 'echo 1;'";

// infinite loop for heroku worker
while (true) {
    echo "Starting process with command `$command`".PHP_EOL;
    passthru($command, $statusCode);
    echo "Process exited with status $statusCode".PHP_EOL;
    sleep(60);
}
