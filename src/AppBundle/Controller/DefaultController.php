<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/hello", name="hello")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function helloAction(Request $request)
    {
        return new Response("Hello, {$this->getUser()->getUsername()} !");
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function loogutAction(Request $request)
    {
    }
}
